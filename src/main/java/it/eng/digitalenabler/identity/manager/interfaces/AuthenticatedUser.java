package it.eng.digitalenabler.identity.manager.interfaces;

import it.eng.digitalenabler.identity.manager.model.User;

public interface AuthenticatedUser {
	User toUser();
}
