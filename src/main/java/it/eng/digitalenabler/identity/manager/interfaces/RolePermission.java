package it.eng.digitalenabler.identity.manager.interfaces;

import it.eng.digitalenabler.identity.manager.model.Permission;

public interface RolePermission {
	Permission toPermission();
}
