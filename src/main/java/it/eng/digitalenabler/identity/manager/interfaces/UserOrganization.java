package it.eng.digitalenabler.identity.manager.interfaces;

import it.eng.digitalenabler.identity.manager.model.Organization;

public interface UserOrganization {
	Organization toOrganization();
}
