package it.eng.digitalenabler.identity.manager.interfaces;

import it.eng.digitalenabler.identity.manager.model.Role;

public interface UserRole {
	Role toRole();
}
