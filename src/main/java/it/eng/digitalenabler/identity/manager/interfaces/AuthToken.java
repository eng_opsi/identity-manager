package it.eng.digitalenabler.identity.manager.interfaces;

import it.eng.digitalenabler.identity.manager.model.Token;

public interface AuthToken {
	Token toToken();
}
