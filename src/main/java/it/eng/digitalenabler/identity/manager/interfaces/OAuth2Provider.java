package it.eng.digitalenabler.identity.manager.interfaces;

import it.eng.digitalenabler.identity.manager.model.Token;
import it.eng.digitalenabler.identity.manager.model.User;

public interface OAuth2Provider {
	public String getAuthorizationCode();
	public Boolean checkToken(String token);
	public Token getAccessToken(String code, String client_id, String client_secret, String redirectUri);
	public Token refreshToken(String refresh_token, String client_id, String client_secret);
	public User getUserInfo(String token);
}
