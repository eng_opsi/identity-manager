package it.eng.digitalenabler.identity.manager.interfaces;

public interface RoleManager {
//	public Set<Role> getUserRoles(String userId);
//	public Set<Organization> getUserOrganizations(String userId);
//	public Set<Role> getUserRoleByOrganizationId(String userId, String organizationId);
	public String getUserOrganizationRole(String userId,String organizationId);
	public void assignUserToOrganization(String orgId, String id, String orgRoleId);
	public void assignUserRole(String appId, String userId, String roleId);
	public String getApplicationRoles(String applicationId);
}
